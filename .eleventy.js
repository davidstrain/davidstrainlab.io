const syntaxHighlight = require("@11ty/eleventy-plugin-syntaxhighlight");
module.exports = function(eleventyConfig) {
    eleventyConfig.addPassthroughCopy("css");
    eleventyConfig.addPassthroughCopy("images");
    eleventyConfig.addPlugin(syntaxHighlight);
    
    return {
        passthroughFileCopy: true,
        dir: {
            output: "public"
        }
    }
  };