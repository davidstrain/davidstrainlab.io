---
noteTitle: Configuring Syntax Highlighting in Eleventy
date: 2020-07-18
tags: 
  - note
  - eleventy
---


Install the eleventy syntax highlighting plugin

``` bash
npm install @11ty/eleventy-plugin-syntaxhighlight --save-dev
```


Add the plugin to your eleventy config file _(.eleventy.js)_
``` js
const syntaxHighlight = require("@11ty/eleventy-plugin-syntaxhighlight");
module.exports = function(eleventyConfig) {
  eleventyConfig.addPlugin(syntaxHighlight);
};
```

Pick a prism theme and add it to your site.  
_(Themes are available from [https://github.com/PrismJS/prism-themes](https://github.com/PrismJS/prism-themes))_

```html
<!doctype html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <title>Random Notes</title>
        <link rel="stylesheet" href="/css/base.css">
        <link rel="stylesheet" href="/css/prism-nord.css">
    </head>

```