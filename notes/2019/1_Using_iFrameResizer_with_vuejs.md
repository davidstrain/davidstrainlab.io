---
noteTitle: Using iframeResizer in a Vue.js Component
date: 2019-09-26
tags: 
  - note
  - vue.js
---


Using the _[iframeresizer](https://github.com/davidjbradshaw/iframe-resizer)_ library in a Vue.js component

```html
<template>
  <div>
    <iframe id="myiframe" :src="http://example.com/url/of/page"></iframe>
  </div>
</template>

<script lang="ts">
import { Component, Vue } from "vue-property-decorator";
import iframeResizer from "iframe-resizer";

@Component({})
export default class IframeComponent extends Vue {
  mounted() {
    this.resize();
  }
  resize() {
    (iframeResizer as any).iframeResizer({ checkOrigin: false }, "#myiframe");
  }
}
</script>
```