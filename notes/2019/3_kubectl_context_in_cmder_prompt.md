---
noteTitle: Add Kubectl Context to Cmder Prompt
date: 2019-10-17
tags: 
  - note
  - kubernetes
  - cmder
---

Add a _.lua_ file under  _.\config\my_config.lua_


```lua
function custom_prompt()
  -- Exectute kubectl command to get context (Note the output will contain a newline)
  local handle = io.popen("kubectl config current-context")
  local context = handle:read("*a")
  handle:close()

  cwd = clink.get_cwd()
  prompt = "\x1b[1;32;40m{cwd} {git}{hg} \x1b[0;35;40m{context}\x1b[1;30;40m{lamb} \x1b[0m"
  new_value = string.gsub(prompt, "{cwd}", cwd)
  add_context = string.gsub(new_value, "{context}", context)
  clink.prompt.value = string.gsub(add_context, "{lamb}", "λ")
end

clink.prompt.register_filter(custom_prompt, 1)
```