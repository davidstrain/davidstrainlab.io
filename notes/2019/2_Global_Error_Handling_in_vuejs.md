---
noteTitle: Global Error Handling in Vue.js
date: 2019-09-26
tags: 
  - note
  - vue.js
---

Handle errors from Vue components
```typescript
 Vue.config.errorHandler = function(err: Error, vm: Vue, info: string) {
      // ...
    };
```

Handle javascript errors
```typescript
 window.onerror = function(msg: any, url: any, line: any, col: any, error: any){
      // ...
    };
```

Handle errors from rejected promises as these are not caught by _window.onerror_
```typescript
 window.addEventListener("unhandledrejection", function(event: any){
      event.preventDefault();
      // ...
    });
```